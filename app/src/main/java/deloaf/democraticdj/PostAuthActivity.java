package deloaf.democraticdj;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.*;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;
import com.spotify.sdk.android.player.AudioController;
import com.spotify.sdk.android.player.Config;
import com.spotify.sdk.android.player.PlayerStateCallback;
import com.spotify.sdk.android.player.Spotify;
import com.spotify.sdk.android.player.ConnectionStateCallback;
import com.spotify.sdk.android.player.Player;
import com.spotify.sdk.android.player.PlayerNotificationCallback;
import com.spotify.sdk.android.player.PlayerState;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class PostAuthActivity
        extends FragmentActivity
        implements View.OnClickListener, PlayerNotificationCallback, ConnectionStateCallback,
        JoinSessionFragment.OnSessionChangedListener, CreateUserFragment.OnUserCreatedListener {
    // TODO: Replace with your client ID
    private static final String CLIENT_ID = "c4b0ee423fae410e93c948010074a399";
    // TODO: Replace with your redirect URI
    private static final String REDIRECT_URI = "djdemocracy-login-spotify://callback";

    private Player mPlayer;
    private int alt = 0;

    private static final int REQUEST_CODE = 42;

    private Track now_playing;
    private List<Track> play_queue = new ArrayList<Track>();
    private List<Integer> queue_ids = new ArrayList<Integer>();
    private Button home, session;
    private int session_or_home = 0; // 0 for session, 1 for home.
    private static final int HOME = 1, SESSION = 0;
    private Session user_session = null;
    private TextView session_name;
    private User user = new User();
    private int user_id;
    private String user_name;
    private Button play_pause_button, skip_song_button;
    private View skip_placeholder, play_pause_placeholder;
    private String base_url = "http://localhost:8080";
//    private String base_url = "intense-sands-7347.herokuapp.com/";
    private SessionLobbyThread active_lobby;
    // private String base_url = "10.0.2.2:5000/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_auth);
        play_pause_button = (Button) findViewById(R.id.play_pause_button);
        skip_song_button = (Button) findViewById(R.id.skip_song_button);
        skip_placeholder = (View) findViewById(R.id.view_skip_placeholder);
        play_pause_placeholder = (View) findViewById(R.id.view_play_pause_placeholder);
        Button add_song = (Button) findViewById(R.id.id_add_song_to_session);
//        home = (Button) findViewById(R.id.button_home);
//        session = (Button) findViewById(R.id.button_session);
        session_name = (TextView) findViewById(R.id.textViewSessionName);
        play_pause_button.setOnClickListener(this);
        skip_song_button.setOnClickListener(this);
        int id_not_found = -1;
        String name_not_found = null;
        SharedPreferences sharedPreferences = getSharedPreferences("djd_democracy.user_preferences", Context.MODE_PRIVATE);
        int found_id = sharedPreferences.getInt("user_id", id_not_found);
        String found_username = sharedPreferences.getString("user_name", name_not_found);
        if ((found_id == id_not_found) && (found_username == null)) { // if no shared prefences exist for the user
            if (findViewById(R.id.main_container) != null && user_session == null) {
                Log.d("user never created", "moving to create user frag");
                CreateUserFragment createUserFragment = new CreateUserFragment();
                createUserFragment.setArguments(getIntent().getExtras());
                getSupportFragmentManager().beginTransaction().add(R.id.main_container, createUserFragment).commit();
            }
        } else if ((found_id != id_not_found) && (found_username != null)) { // if shared preferences exist for the user
            Log.d("found user", found_id + " :: " + found_username);
            Log.d("user already created", "moving to join session frag");
            Log.d("found user info", found_id + " : " + found_username);
            user_id = found_id;
            user_name = found_username;
            if (findViewById(R.id.main_container) != null && user_session == null) {
                JoinSessionFragment joinSessionFragment = new JoinSessionFragment ();
                Bundle bundle = new Bundle();
                bundle.putInt("user_id", found_id);
                bundle.putString("user_name", found_username);
                joinSessionFragment.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.main_container, joinSessionFragment).commit();
            }
        } else {
            Log.d("user maybe created?", "error writing to shared preferences");
        }

//        home.setOnClickListener(this);
//        session.setOnClickListener(this);
//        if (findViewById(R.id.main_container) != null && user_session == null)
//        {
//            JoinSessionFragment joinSessionFragment = new JoinSessionFragment ();
//            joinSessionFragment.setArguments(getIntent().getExtras());
//            getSupportFragmentManager().beginTransaction().add(R.id.main_container, joinSessionFragment).commit();
//        }

        AuthenticationRequest.Builder builder = new AuthenticationRequest.Builder(CLIENT_ID, AuthenticationResponse.Type.TOKEN, REDIRECT_URI);

        builder.setScopes(new String[]{"user-read-private", "streaming"});
        AuthenticationRequest req = builder.build();
        AuthenticationClient.openLoginActivity(this, REQUEST_CODE, req);

    }

    @Override
    protected void onPause() {
        super.onPause();
        super.onResume();
//        if (mPlayer != null)
//            mPlayer.resume();
//            mPlayer.
        if (active_lobby != null) {
            active_lobby.onPause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (active_lobby != null) {
            active_lobby.onResume();
        }
    }

    public void onSessionCreated(Session session) {
        Log.d("on session created", "session passed back to activity");
        user_session = session;
        user_session.setHost(true); // created the session, so user is the host and will be playing music.
        Log.d("session id", String.valueOf(user_session.getSession_id()));
        Log.d("session name", user_session.getSession_name());
        session_name.setText(user_session.getSession_name());
        session_name.setVisibility(View.VISIBLE);
        SessionFragment sessionFragment = new SessionFragment();
        Bundle b = new Bundle();
        b.putInt("session_id", user_session.getSession_id());
        b.putString("session_name", user_session.getSession_name());
        sessionFragment.setArguments(b);
        getSupportFragmentManager().beginTransaction().replace(R.id.main_container, sessionFragment).commit();
        getSupportFragmentManager().executePendingTransactions();



        Log.d("on session created", "lobby thread starting");
        active_lobby = new SessionLobbyThread(user_session);
        Log.d("on session created", "lobby thread created");
        active_lobby.setActivity(this);
        Log.d("on session created", "thread activity set");
        active_lobby.start();
        Log.d("on session created", "thread running");
        //launch thread that gets session play queue
    }

    public void onSessionJoined(Session session) {
        Log.d("on session joined", "session passed back to activity");
        user_session = session;
        user_session.setHost(false); // joined the session, so not the host and should not be playing any sort of music
        Log.d("session id", String.valueOf(user_session.getSession_id()));
        Log.d("session name", user_session.getSession_name());
        session_name.setText(user_session.getSession_name());
        session_name.setVisibility(View.VISIBLE);
        SessionFragment sessionFragment = new SessionFragment();
        Bundle b = new Bundle();
        b.putInt("session_id", user_session.getSession_id());
        b.putString("session_name", user_session.getSession_name());
        sessionFragment.setArguments(b);
        getSupportFragmentManager().beginTransaction().replace(R.id.main_container, sessionFragment).commit();

        Log.d("on session joined", "lobby thread starting");
        active_lobby = new SessionLobbyThread(user_session);
        Log.d("on session joined", "lobby thread created");
        active_lobby.setActivity(this);
        Log.d("on session joined", "thread activity set");
        active_lobby.start();
        Log.d("on session joined", "thread running");
        // launch thread that gets session play queue
    }

    public void onUserCreated(User u) {
        user = new User(u.getUser_id(), u.getUser_name());
        Log.d("on user created", "user parameter u: " + u.getUser_id() + ", " + u.getUser_name());
        if (findViewById(R.id.main_container) != null && user_session == null)
        {
            Log.d("user just created", "moving to join session frag");
            JoinSessionFragment joinSessionFragment = new JoinSessionFragment ();
            Bundle bundle = new Bundle();
            bundle.putInt("user_id", user.getUser_id());
            bundle.putString("user_name", user.getUser_name());
            joinSessionFragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().replace(R.id.main_container, joinSessionFragment).commit();
        }
    }

    @Override
    public void onClick(View v) {
        int user_id = 2;
        switch (v.getId()) {
            case R.id.play_pause_button:


                if (alt == 0) {
                    v.setBackground(getResources().getDrawable(R.drawable.play_button));
                    Log.d("play back", "music paused");
                    mPlayer.pause();
                    alt = 1;
                } else if (alt == 1) {
                    v.setBackground(getResources().getDrawable(R.drawable.pause_button));
                    Log.d("play back", "music resumed");
                    mPlayer.resume();
                    alt = 0;
                }
                break;

//            case R.id.id_add_song_to_session:
//                Log.d("add song", "before bundle addition");
//
//                Intent intent = new Intent(this, SearchActivity.class);
//                intent.putExtra("session_id", user_session.getSession_id());
//                Log.d("add song", "after bundle addition");
//
//                startActivity(intent);
//                break;
            case R.id.skip_song_button:
                if (play_queue.size() > 0) {
                    Track next = play_queue.get(0);
                    mPlayer.play("spotify:track:" + next.getSpotifyID());
                    play_queue.remove(0);
//            queue_ids.remove(0);
                    removeSongFromQueue(next);
                    setQueueOnUI(play_queue);
                    setNowPlayingOnUI(next);
                    now_playing = next;
                    setNowPlayingAsync();
                    //getPlayQueue(user_session.getSession_id());
                    // decide when / how often you want the app to query for an updated song queue. Maybe get the next 10 songs each time, and
                    // if the queue is four or less, you want to queue for more songs.
                } else {
                    String toast_text = "there is no song to skip to";
                    Toast toast = Toast.makeText(getApplicationContext(), toast_text, Toast.LENGTH_SHORT);
                    toast.show();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, Intent intent) {
        Log.d("Spotify", "after authentication");
        super.onActivityResult(requestCode, resultCode, intent);

        // Check if result comes from the correct activity
        if (requestCode == REQUEST_CODE) {
            AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, intent);
            Log.d("Spotify", "request code is correct");
            switch (response.getType()) {
                // Response was successful and contains auth token
                case TOKEN:
                    // Handle successful response
                    Log.d("Spotify", "Successful response");
                    Config playerConfig = new Config(this, response.getAccessToken(), CLIENT_ID);
//                    AudioController ac = new AudioController() {
//                    }
                    Log.d("Spotify", "Config created");


                    final String auth_token = response.getAccessToken();
                    final String client_id = CLIENT_ID;
                    try {
                        mPlayer = Spotify.getPlayer(playerConfig, this, new Player.InitializationObserver() {

                            @Override
                            public void onInitialized(Player player)
                            {
                                Log.d("Spotify", "onInitialized method");
                                mPlayer.addConnectionStateCallback(PostAuthActivity.this);
                                mPlayer.addPlayerNotificationCallback(PostAuthActivity.this);
//                                Log.d("Spotify Activity", "launching post auth activity");
//                                mPlayer.play("spotify:track:5U2NrRZfZeMXAzlVwU389t");
//                                int session_id = 92;
//                                getPlayQueue(session_id);

                            }

                            @Override
                            public void onError(Throwable throwable) {
                                Log.e("MainActivity", "Could not initialize player: " + throwable.getMessage());
                            }
                        });
                    } catch (Exception e) {
                        Log.d("Spotify", "Ain't that some shit");
                    }
                    break;

                // Auth flow returned an error
                case ERROR:
                    // Handle error response
                    Log.d("Spotify", "Unsuccessful response");
                    break;

                // Most likely auth flow was cancelled
                default:
                    Log.d("Spotify", "Default response");
                    // Handle other cases
            }
        }
    }

    private void mainPlayerThread() {
        // get the play queue

        // while song in queue
            // play first song
    }

    @Override
    public void onLoggedIn()
    {
        Log.d("MainActivity", "User logged in");
    }

    @Override
    public void onLoggedOut() {
        Log.d("MainActivity", "User logged out");
    }

    @Override
    public void onLoginFailed(Throwable error) {
        Log.d("MainActivity", "Login failed");
    }

    @Override
    public void onTemporaryError() {
        Log.d("MainActivity", "Temporary error occurred");
    }

    @Override
    public void onConnectionMessage(String message) {
        Log.d("MainActivity", "Received connection message: " + message);
    }

    @Override
    public void onPlaybackEvent(PlayerNotificationCallback.EventType eventType, PlayerState playerState) {
        if (eventType == EventType.TRACK_END) { // should update queue and now playing and remove from DB
            if (play_queue.size() > 0) {
                Track next = play_queue.get(0);
                mPlayer.play("spotify:track:" + next.getSpotifyID());
                play_queue.remove(0);
//            queue_ids.remove(0);
                removeSongFromQueue(next);
                setQueueOnUI(play_queue);
                setNowPlayingOnUI(next);
                now_playing = next;
                setNowPlayingAsync();
                //getPlayQueue(user_session.getSession_id());
                // decide when / how often you want the app to query for an updated song queue. Maybe get the next 10 songs each time, and
                // if the queue is four or less, you want to queue for more songs.
            }
            // here, add else in order to setActive(false), if the below if statement does not already do so.
        }

        if (playerState.playing) {
            Log.d("play back event", "playing = true");
            user_session.setActive(true);
        }

        if (!playerState.playing && play_queue.size() == 0) {
            Log.d("play back event", "playing = false");
            user_session.setActive(false);
        }
    }

    @Override
    public void onPlaybackError(PlayerNotificationCallback.ErrorType errorType, String errorDetails) {
        Log.d("MainActivity", "Playback error received: " + errorType.name());
    }

    @Override
    protected void onDestroy() {
        Spotify.destroyPlayer(this);
        super.onDestroy();
    }

    public boolean isSessionActive()
    {
        return user_session.isActive();
    }

    public void start() {

        Log.d("starting player", "starting player");
        if (play_queue.size() > 0) {

            final Track next = play_queue.get(0);
            mPlayer.play("spotify:track:" + next.getSpotifyID());
            Log.d("starting player", "starting player");
            play_queue.remove(0);
            // here, need to query to remove the song from the DB
            removeSongFromQueueSync(next);
            now_playing = next;
            setNowPlaying();
            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    skip_placeholder.setVisibility(View.GONE);
                    play_pause_placeholder.setVisibility(View.GONE);
                    skip_song_button.setVisibility(View.VISIBLE);
                    play_pause_button.setVisibility(View.VISIBLE);
                    skip_song_button.setBackground(getResources().getDrawable(R.drawable.pause_button));
                    play_pause_button.setBackground(getResources().getDrawable(R.drawable.pause_button));
                    setQueueOnUI(play_queue);
                    setNowPlayingOnUI(next);
                }
            });
        }
    }

    public void getPlayQueue(int session_id) {

        AsyncHttpClient client = new AsyncHttpClient();
        String url = base_url + "getsessionqueue/" + session_id + "/";
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                List<Track> queue;
                String response = new String(responseBody);
                try {
                    JSONObject obj = new JSONObject(response);
                    Log.d("retrieve queue", response);
                    queue = parseJSONIntoQueue(obj);
                    setQueueOnUI(queue);
                    play_queue = queue;
                } catch (JSONException e) {
                    Log.e("str to json", e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d("retrieve queue", error.getMessage());
            }
        });

    }

    public void setNowPlayingAsync() {
        AsyncHttpClient client = new AsyncHttpClient();
        String url = base_url + "setnowplaying/" + user_session.getSession_id() + "/" + now_playing.song_name + "/" + now_playing.artist_name + "/";
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                try {
                    JSONObject response = new JSONObject(new String(responseBody));
                    JSONObject session_set = response.getJSONObject("session");
                    String sn, an;
                    sn = session_set.getString("song_name");
                    an = session_set.getString("artist_name");
                    if (sn == now_playing.song_name && an == now_playing.artist_name) {
                        Log.d("set now playing", "ASYNC SUCCESS");
                        Log.d("info set", sn + " by " + an);
                        Log.d("should be", now_playing.song_name + " by " + now_playing.artist_name);

                    } else {
                        Log.d("set now playing", "ASYNC FAILURE");
                    }
                }
                catch (JSONException e) {
                    Log.e("str to json", e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d("retrieve queue", error.getMessage());
            }
        });
    }

    public void setNowPlaying() {
        SyncHttpClient sync_client = new SyncHttpClient();
        String url = base_url + "setnowplaying/" + user_session.getSession_id() + "/" + now_playing.song_name + "/" + now_playing.artist_name + "/";
        Log.d("set now playing", url);
        sync_client.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONObject session_set = response.getJSONObject("session");
                    String sn, an;
                    sn = session_set.getString("song_name");
                    an = session_set.getString("artist_name");
                    if (sn == now_playing.song_name && an == now_playing.artist_name) {
                        Log.d("set now playing", "SYNC SUCCESS");
                        Log.d("info set", sn + " by " + an);
                        Log.d("should be", now_playing.song_name + " by " + now_playing.artist_name);

                    } else {
                        Log.d("set now playing", "SYNC FAILURE");
                    }
                }
                catch (JSONException e) {

                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject error) {
                Log.d("set now playing error", "some json object error");
            }
        });
    }

    public void getQueueIdsSync(final int session_id) {
//        final List<Integer> queue_ids_copy = queue_ids;
        Log.d("track ids", "before requeust size: " + queue_ids.size());
        for (Integer i : queue_ids) {
            Log.d("track id", String.valueOf(i));
        }
        SyncHttpClient sync_client = new SyncHttpClient();
        String ids_url = base_url + "getqueueids/" + session_id + "/";
        sync_client.get(ids_url, new JsonHttpResponseHandler() {
            List<Integer> new_queue_ids = new ArrayList<Integer>();
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray tracks = response.getJSONArray("tracks");
                    for (int i = 0; i < tracks.length(); i++) {
                        int id = tracks.getJSONObject(i).getInt("id");
                        new_queue_ids.add(id);
                    }
                    Log.d("track ids", "after requeust size: " + new_queue_ids.size());for (Integer i : new_queue_ids)
                    {
                        Log.d("track id", String.valueOf(i));
                    }
                    if (!new_queue_ids.equals(queue_ids)) {
                        Log.d("get queue ids", "queue id list different, getting updated track info");
                        queue_ids = new_queue_ids;
                        getPlayQueueSync(session_id);
                        Log.d("get queue ids", "getPlayQueue called");
                    } else {
                        Log.d("get queue ids", "queue id list same");
                    }
                }
                catch (JSONException e) {
                    Log.e("get queue ids", e.getMessage());
                }





//                Log.d("retrieve queue", response.toString());
//                final List<Track> queue = parseJSONIntoQueue(response);
//                runOnUiThread(new Runnable()
//                {
//                    @Override
//                    public void run()
//                    {
//                        setQueueOnUI(queue);
//                    }
//                });
//                play_queue = queue;

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject error) {
                Log.d("retrieve queue error", "some json object error");
            }
        });
    }

    public void getPlayQueueSync(int session_id) {

        SyncHttpClient sync_client = new SyncHttpClient();


        Log.d("getPlayQueueSync", "method called");

        String url = base_url + "getsessionqueue/" + session_id + "/";
        Log.d("get play queue", url);
        sync_client.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                Log.d("retrieve queue", response.toString());
                final List<Track> queue = parseJSONIntoQueue(response);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setQueueOnUI(queue);
                    }
                });
                play_queue = queue;

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject error) {
                Log.d("retrieve queue error", "some json object error");
            }
        });


    }

    public void getNowPlayingSync(int session_id) {
        SyncHttpClient sync_client = new SyncHttpClient();
        String url = base_url + "getnowplaying/" + session_id + "/";
//        String url = base_url + "sessions/";
        Log.d("now playing url", url);
        if (!user_session.isHost()) {
            Log.d("is host", "USER IS NOT HOST");
            sync_client.get(url, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    Log.d("now playing success", response.toString());
                    try {
                        JSONObject session_set = response.getJSONObject("session");
                        final String sn = session_set.getString("song_name");
                        final String an = session_set.getString("artist_name");
                        Log.d("now playing", sn + " by " + an);
                        now_playing = new Track(an, sn);
                        runOnUiThread(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                Log.d("get now playing", "setting now playing on the UI");
                                setNowPlayingOnUI(now_playing);
                            }
                        });

                    }
                    catch (JSONException e) {
                        Log.d("now playing", "some json exception");
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject error) {
                    Log.d("get now playing error", "some json object error");
                }
            });
        }
    }

    private List<Track> parseJSONIntoQueue(JSONObject json_queue) {
        List<Track> tracks = new ArrayList<Track>();
        try {
            JSONArray items = json_queue.getJSONArray("tracks");
            for (int i = 0; i < items.length(); i++) {
                int id = items.getJSONObject(i).getInt("id");
                String song_id = items.getJSONObject(i).getString("spotify_id");
                String song_name = items.getJSONObject(i).getString("song_name");
                String artist_name = items.getJSONObject(i).getString("artist_name");
                Track track = new Track(artist_name, song_name, song_id, id);
                tracks.add(track);
            }
        } catch (JSONException e) {
            Log.e("parse json response", e.getMessage());
        }

        return tracks;
    }

    private Session createSessionFromJSON(JSONObject response) {
        Session session = null;
        try {
            JSONArray items = response.getJSONArray("items");
            for (int i = 0; i < items.length(); i++) {
                int id = items.getJSONObject(i).getInt("id");
                String session_name = items.getJSONObject(i).getString("session_name");
                session = new Session(id, session_name);
                Log.d("id", String.valueOf(session.getSession_id()));
                Log.d("name", session.getSession_name());
            }
        } catch (JSONException e) {
            Log.e("parse json response", e.getMessage());
        }
        return session;
    }

    private void setNowPlayingOnUI(Track now_playing) {
        TextView song_name = (TextView) findViewById(R.id.now_playing_song);
        TextView artist_name = (TextView) findViewById(R.id.now_playing_artist);

        song_name.setText(now_playing.getSongName());
        artist_name.setText(now_playing.getArtistName());
    }

    private void setQueueOnUI(List<Track> queue) {

        Log.d("set queue", "setting queue");
        TextView song1 = (TextView) findViewById(R.id.songQueue1);

        TextView artist1 = (TextView) findViewById(R.id.artistQueue1);
        TextView song2 = (TextView) findViewById(R.id.songQueue2);
        TextView artist2 = (TextView) findViewById(R.id.artistQueue2);
        TextView song3 = (TextView) findViewById(R.id.songQueue3);
        TextView artist3 = (TextView) findViewById(R.id.artistQueue3);
        TextView song4 = (TextView) findViewById(R.id.songQueue4);
        TextView artist4 = (TextView) findViewById(R.id.artistQueue4);
        TextView song5 = (TextView) findViewById(R.id.songQueue5);
        TextView artist5 = (TextView) findViewById(R.id.artistQueue5);
        ArrayList<TextView> artist_list = new ArrayList<TextView>();
        ArrayList<TextView> song_list = new ArrayList<TextView>();
        song_list.add(song1);
        song_list.add(song2);
        song_list.add(song3);
        song_list.add(song4);
        song_list.add(song5);
        artist_list.add(artist1);
        artist_list.add(artist2);
        artist_list.add(artist3);
        artist_list.add(artist4);
        artist_list.add(artist5);

        if (queue.size() == 0) {
            Log.d("set queue ui", "empty queue");
            song1.setText("your next song here");
            song2.setText("");
            song3.setText("");
            song4.setText("");
            song5.setText("");
            artist1.setText("");
            artist2.setText("");
            artist3.setText("");
            artist4.setText("");
            artist5.setText("");
        }

        for (int i = 0; i < 5; i++) { // i < queue.size(); i++)
            if (i >= queue.size()) {
                song_list.get(i).setText("");
                artist_list.get(i).setText("");
            } else {
                Track t = queue.get(i);
                song_list.get(i).setText(t.getSongName());
                artist_list.get(i).setText(t.getArtistName());
                Log.d("set queue", String.valueOf(t.getId()));
                Log.d("set queue", t.getSpotifyID());
            }
        }
    }

    private void removeSongFromQueue(Track song) {
        AsyncHttpClient client = new AsyncHttpClient();
        String url = base_url + "removesongfromsession/" + song.getId() + "/";
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                List<Track> queue;
                String response = new String(responseBody);
                Log.d("remove song", response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.d("remove song", error.getMessage());
            }
        });
    }

    private void removeSongFromQueueSync(Track song) {
        SyncHttpClient sync_client = new SyncHttpClient();
        String url = base_url + "removesongfromsession/" + String.valueOf(song.getId()) + "/";
        Log.d("remove song", url);
        sync_client.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                Log.d("remove song", response.toString());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject error) {
                Log.d("remove song", "failed to remove");
            }
        });
    }

//    public void getPlayQueueSync(int session_id)
//    {
//        SyncHttpClient sync_client = new SyncHttpClient();
//        String url = "http://10.0.2.2:5000/getsessionqueue/" + session_id + "/";
//        Log.d("get play queue", url);
//        sync_client.get(url, new JsonHttpResponseHandler()
//        {
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, JSONObject response)
//            {
//
//                Log.d("retrieve queue", response.toString());
//                final List<Track> queue = parseJSONIntoQueue(response);
//                runOnUiThread(new Runnable()
//                {
//                    @Override
//                    public void run()
//                    {
//                        setQueueOnUI(queue);
//                    }
//                });
//                play_queue = queue;
//
//            }
//
//            @Override
//            public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject error)
//            {
//                Log.d("retrieve queue error", "some json object error");
//            }
//        });
//    }

    public Session getSession()
    {
        return user_session;
    }

    public void setSession(Session session)
    {
        user_session = session;
    }

    public void setQueue(List<Track> updated_queue)
    {
        play_queue = updated_queue;
    }
}

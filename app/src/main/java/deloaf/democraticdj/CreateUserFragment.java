package deloaf.democraticdj;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by mitchkeith145 on 6/12/15.
 */
public class CreateUserFragment extends Fragment implements View.OnClickListener
{
    private OnUserCreatedListener mCallback;
    private EditText username, email;
    private String base_url = "intense-sands-7347.herokuapp.com/";
    // private String base_url = "10.0.2.2:5000/";
    public interface OnUserCreatedListener
    {
        void onUserCreated(User user);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_create_user, container, false);

        username = (EditText) v.findViewById(R.id.editTextUsername);
        email = (EditText) v.findViewById(R.id.editTextEmail);
        Button create = (Button) v.findViewById(R.id.button_create_user);
        create.setOnClickListener(this);

        return v;
    }

    @Override
    public void onAttach(Activity myActivity)
    {
        super.onAttach(myActivity);

        try
        {
            mCallback = (OnUserCreatedListener) myActivity;
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(myActivity.toString() + " must implement OnUserCreated");
        }
    }
    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.button_create_user:
                String un = username.getText().toString();
                String em = email.getText().toString();
                AsyncHttpClient client = new AsyncHttpClient();
                String url = "https://" + base_url + "newuser/" + un + "/" + em + "/";
                Log.d("create user", url);
                client.get(url, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        List<Track> queue;
                        String response = new String(responseBody);
                        try
                        {

                            JSONObject obj = new JSONObject(response);
                            int id = obj.getInt("id");
                            String username = obj.getString("username");
                            Log.d("user data", id + " : " + username);

                            if (id > 0 && username.length() > 0) // ids, username are valid
                            {
                                User user = new User(id, username);
                                Log.d("user object", user.getUser_id() + " : " + user.getUser_name());
                                Log.d("create user", "user object created");
                                Context context = getActivity();
                                SharedPreferences sharedPreferences = context.getSharedPreferences("djd_democracy.user_preferences", Context.MODE_PRIVATE);
                                Log.d("create user", "opening shared preferences file");
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                Log.d("create user", "opening shared preferences editor");
                                editor.putInt("user_id", user.getUser_id());
                                editor.putString("user_name", user.getUser_name());
                                Log.d("create user", "put id and user name to file, about to commit");
                                editor.commit();
                                Log.d("create user", "committed to file, calling back to activity");
                                mCallback.onUserCreated(user);

                            }

                        }
                        catch (JSONException e)
                        {
                            Log.e("str to json", e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        Log.d("create user", error.getMessage());
                    }
                });
        }
    }
}

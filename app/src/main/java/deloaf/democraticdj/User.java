package deloaf.democraticdj;

/**
 * Created by mitchkeith145 on 6/12/15.
 */
public class User
{
    private int user_id;
    private String user_name;

    public User(int id, String name)
    {
        user_id = id;
        user_name = name;
    }

    public User()
    {
        user_name = null;
        user_id = -1;
    }

    public int getUser_id()
    {
        return user_id;
    }

    public String getUser_name()
    {
        return user_name;
    }

    public void setUser_id(int id)
    {
        user_id = id;
    }

    public void setUser_name(String name)
    {
        user_name = name;
    }
}

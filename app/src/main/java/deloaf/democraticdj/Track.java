package deloaf.democraticdj;

/**
 * Created by mitchkeith145 on 6/8/15.
 */
public class Track
{
    String artist_name, song_name, spotify_id;
    int id;

    public Track(String an, String sn, String si, int db_id)
    {
        artist_name = an;
        song_name = sn;
        spotify_id = si;
        id = db_id;
    }

    public Track(String an, String sn)
    {
        song_name = sn;
        artist_name = an;
    }

    public String getSpotifyID()
    {
        return spotify_id;
    }

    public String getSongName()
    {
        return song_name;
    }

    public String getArtistName()
    {
        return artist_name;
    }

    public int getId()
    {
        return id;
    }
}

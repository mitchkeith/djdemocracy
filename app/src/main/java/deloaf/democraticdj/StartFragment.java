package deloaf.democraticdj;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by Mitch on 5/4/2015.
 */
public class StartFragment extends Fragment implements View.OnClickListener
{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_start, container, false);
        Button start = (Button) v.findViewById(R.id.buttonStart);

        start.setOnClickListener(this);

        return v;
    }

    public void onClick(View v)
    {
        Intent fire_new_activity;
        switch (v.getId())
        {
            case R.id.buttonStart:
                fire_new_activity = new Intent(this.getActivity(), SearchActivity.class);
                startActivity(fire_new_activity);
                Activity a = getActivity();
                Context context = a.getApplicationContext();
                CharSequence text = "start button pushed";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                break;
        }
    }
}

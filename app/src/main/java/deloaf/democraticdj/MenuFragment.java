package deloaf.democraticdj;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

/**
 * Created by Mitch on 5/4/2015.
 */
public class MenuFragment extends Fragment implements View.OnClickListener
{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_header_menu, container, false);
        LinearLayout menu = (LinearLayout) v.findViewById(R.id.buttonMenu);
        Button login = (Button) v.findViewById(R.id.buttonLogin);
        menu.setOnClickListener(this);
        login.setOnClickListener(this);
        return v;
    }

    public void onClick(View v)
    {
        Intent fire_new_activity;
        switch (v.getId())
        {
            case R.id.buttonMenu:
//                fire_new_activity = new Intent(this.getActivity(), JoinSession.class);
//                startActivity(fire_new_activity);
                Activity a = getActivity();
                Context context = a.getApplicationContext();
                CharSequence text = "menu button pushed";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                break;
            case R.id.buttonLogin:
//                fire_new_activity = new Intent(this.getActivity(), JoinSession.class);
//                startActivity(fire_new_activity);
                a = getActivity();
                context = a.getApplicationContext();
                text = "login button pushed";
                duration = Toast.LENGTH_SHORT;
                toast = Toast.makeText(context, text, duration);
                toast.show();
                Intent intent = new Intent(this.getActivity(), SpotifyActivity.class);
                startActivity(intent);
                break;
        }
    }
}


package deloaf.democraticdj;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.wrapper.spotify.Api;
import com.wrapper.spotify.exceptions.WebApiException;
import com.wrapper.spotify.methods.TrackSearchRequest;
import com.wrapper.spotify.models.*;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mitchkeith145 on 6/17/15.
 */
public class SearchFragment extends Fragment implements View.OnClickListener
{
    LinearLayout container_add_song, search_results_layout;
    Button add_song, nevermind_song, search;
    EditText edit_text_search;
    View white_space;
    private int session_id;
    private String session_name;
    private String base_url = "intense-sands-7347.herokuapp.com/";
    // private String base_url = "10.0.2.2:5000/";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_search, container, false);
        session_id = getArguments().getInt("session_id");
        session_name = getArguments().getString("session_name");
        container_add_song = (LinearLayout) v.findViewById(R.id.container_add_song);
        add_song = (Button) v.findViewById(R.id.button_add_song);
        nevermind_song = (Button) v.findViewById(R.id.button_nevermind);
        search = (Button) v.findViewById(R.id.buttonSearch);
        edit_text_search = (EditText) v.findViewById(R.id.editTextSearch);
        white_space = (View) v.findViewById(R.id.view_white_space);
        search_results_layout = (LinearLayout) v.findViewById(R.id.linearLayoutResults);
//        add_song.setOnClickListener(this);
//        nevermind_song.setOnClickListener(this);
        search.setOnClickListener(this);


        return v;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonSearch:
                String search_text = edit_text_search.getText().toString();
                if (search_text.length() == 0) {
                    String toast_text = "your search is empty";
                    Toast toast = Toast.makeText(getActivity().getApplicationContext(), toast_text, Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    new Thread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            Page<com.wrapper.spotify.models.Track> tracks = new Page<com.wrapper.spotify.models.Track>();
                            Api api = Api.DEFAULT_API;
                            Log.d("SearchActivity", "one");
                            TrackSearchRequest.Builder track_search = api.searchTracks(edit_text_search.getText().toString());
                            Log.d("SearchActivity", "two");
                            TrackSearchRequest result = track_search.build();
                            Log.d("SearchActivity", "three");
                            try
                            {
                                tracks = result.get();
                            }
                            catch (IOException e)
                            {
                                Log.e("IOException", e.getMessage());
                            }
                            catch (WebApiException e)
                            {
                                Log.e("WebApiException", e.getMessage());
                            }
//                            Log.d("Search Activity", Integer.toString(tracks.getTotal()));
                            List<com.wrapper.spotify.models.Track> track_results = tracks.getItems();

                            final List<Button> track_list = new ArrayList<Button>();
                            for (com.wrapper.spotify.models.Track t : track_results)
                            {
                                String artist = "";
                                for (SimpleArtist a : t.getArtists())
                                {
                                    artist += a.getName();
                                }
                                final String final_artist = artist;
                                final String track_name = t.getName();
                                final String track_id = t.getId();
                                final String album = t.getAlbum().getName();
                                try
                                {
                                    Log.d("Search Activity", t.getName() + " by " + artist + " from " + t.getAlbum().getName());
                                }
                                catch (Exception e)
                                {
                                    Log.d("Search Activity", e.getMessage());
                                }
                                final Button track = new Button(getActivity().getApplicationContext());

                                track.setOnClickListener(new View.OnClickListener()
                                {
                                    @Override
                                    public void onClick(View view)
                                    {
                                        container_add_song.setVisibility(View.VISIBLE);
                                        white_space.setVisibility(View.VISIBLE);
                                        add_song.setOnClickListener(new View.OnClickListener() {
                                            public void onClick(View view) {
                                                AsyncHttpClient client = new AsyncHttpClient();
                                                String url = "https://" + base_url + "addsongtosession/" + session_id + "/" + track_id + "/";
                                                Log.d("find session loc", url);
                                                client.get(url, new AsyncHttpResponseHandler() {
                                                    @Override
                                                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                                        String response = new String(responseBody);
                                                        Log.d("Search activity", "Response is: " + response);
                                                        Toast toast = Toast.makeText(getActivity().getApplicationContext(), "successfully added song", Toast.LENGTH_SHORT);
                                                        toast.show();
//                                                        getActivity().getSupportFragmentManager().popBackStackImmediate();
                                                        SessionFragment sessionFragment = new SessionFragment ();
                                                        sessionFragment.setArguments(getArguments());
                                                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_container, sessionFragment).commit();
                                                    }

                                                    @Override
                                                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                                        Log.d("Search activity", "failure: " + error.getMessage());
                                                        Toast toast = Toast.makeText(getActivity().getApplicationContext(), "failed to add song", Toast.LENGTH_SHORT);
                                                        toast.show();
                                                    }
                                                });
                                            }
                                        });
                                        nevermind_song.setOnClickListener(new View.OnClickListener()
                                        {
                                            @Override
                                            public void onClick(View view)
                                            {
                                                container_add_song.setVisibility(View.GONE);
                                                white_space.setVisibility(View.VISIBLE);
                                                for (Button button : track_list)
                                                {
                                                    button.setVisibility(View.VISIBLE);
                                                }
                                            }
                                        });
                                        Log.d("search fragment", track_name + " by " + final_artist + " from " + album + " clicked.");
                                        for (Button button : track_list)
                                        {
                                            if (button != track)
                                            {
                                                button.setVisibility(View.GONE);
                                            }
                                        }
                                    }
                                });
                                final LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                track.setText(t.getName() + " by " + artist + " from " + t.getAlbum().getName());
                                track.setTextColor(getResources().getColor(R.color.white));
                                track.setBackgroundColor(getResources().getColor(R.color.background));
//                                search_results_layout.addView(track, lp);
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        search_results_layout.addView(track, lp);
                                    }
                                });
                                track_list.add(track);
                            }
                        }
                    }).start();
                }
                break;

//            case R.id.button_add_song:
//
//                break;
//
//            case R.id.button_nevermind:
//
//                break;
        }
    }
}

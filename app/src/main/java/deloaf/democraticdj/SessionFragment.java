package deloaf.democraticdj;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * Created by mitchkeith145 on 6/3/15.
 */
public class SessionFragment extends Fragment implements View.OnClickListener
{

//    private Session user_session;
    private PostAuthActivity attachedActivity;
    private int session_id;
    private String session_name;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_session_dup, container, false);
//        Bundle b = getArguments();
//        int session_id = b.getInt("session_id");
//        String session_name = b.getString("session_name");
//        user_session = new Session(session_id, session_name);
//        Log.d("session info", user_session.getSession_name() + " : " + String.valueOf(user_session.getSession_id()));
        Button add_song = (Button) v.findViewById(R.id.id_add_song_to_session);
        add_song.setOnClickListener(this);
        session_id = getArguments().getInt("session_id");
        session_name = getArguments().getString("session_name");
        Log.d("session fragment", "session " + session_id + " " + session_name);
//        Log.d("on session created", "lobby thread starting");
//        SessionLobbyThread active_lobby = new SessionLobbyThread(user_session);
//        Log.d("on session created", "lobby thread created");
//        active_lobby.setActivity((PostAuthActivity) getActivity());
//        Log.d("on session created", "thread activity set");
//        active_lobby.start();
//        Log.d("on session created", "thread running");
        return v;
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.id_add_song_to_session:
                Bundle bundle = new Bundle();
                bundle.putInt("session_id", session_id);
                bundle.putString("session_name", session_name);
                Log.d("launching search", "putting args");
//                Intent intent = new Intent(this.getActivity(), SearchActivity.class);
//                intent.putExtras(bundle);
//                startActivity(intent);

                SearchFragment searchFragment = new SearchFragment ();
//                Bundle bundle = new Bundle();
//                bundle.putInt("user_id", user.getUser_id());
//                bundle.putString("user_name", user.getUser_name());
                searchFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.main_container, searchFragment).commit();
        }
    }

    @Override
    public void onAttach(Activity myActivity)
    {
        super.onAttach(myActivity);
    }
}

package deloaf.democraticdj;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.wrapper.spotify.Api;
import com.wrapper.spotify.exceptions.WebApiException;
import com.wrapper.spotify.methods.TrackSearchRequest;
import com.wrapper.spotify.models.Artist;
import com.wrapper.spotify.models.Page;
import com.wrapper.spotify.models.SimpleArtist;
import com.wrapper.spotify.models.Track;
import com.google.common.util.concurrent.SettableFuture;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


public class SearchActivity extends Activity implements View.OnClickListener
{
    Button button_search;
    EditText edit_text_search;
    LinearLayout search_results_layout;
    ScrollView scrollView_results;
    private int session_id;
    private String base_url = "intense-sands-7437.herokuapp.com/";
    // private String base_url = "10.0.2.2:5000/";
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        if (getIntent().getExtras() != null)
            session_id = getIntent().getExtras().getInt("session_id");
        Log.d("search activity", "session id: " + session_id);
        button_search = (Button) findViewById(R.id.buttonSearch);
        edit_text_search = (EditText) findViewById(R.id.editTextSearch);
        search_results_layout = (LinearLayout) findViewById(R.id.linearLayoutResults);
        scrollView_results = (ScrollView) findViewById(R.id.scrollViewResults);
        button_search.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v)
    {

        Context context = getApplicationContext();
        switch (v.getId())
        {
            case R.id.buttonSearch:
                String search_text = edit_text_search.getText().toString();
                if (search_text.length() == 0)
                {
                    String toast_text = "your search is empty";
                    Toast toast = Toast.makeText(context, toast_text, Toast.LENGTH_SHORT);
                    toast.show();
                }
                else
                {

                    new Thread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            Page<Track> tracks = new Page<Track>();
                            Api api = Api.DEFAULT_API;
                            Log.d("SearchActivity", "one");
                            TrackSearchRequest.Builder track_search = api.searchTracks(edit_text_search.getText().toString());
                            Log.d("SearchActivity", "two");
                            TrackSearchRequest result = track_search.build();
                            Log.d("SearchActivity", "three");
                            try
                            {
                                tracks = result.get();
                            }
                            catch (IOException e)
                            {
                                Log.e("IOException", e.getMessage());
                            }
                            catch (WebApiException e)
                            {
                                Log.e("WebApiException", e.getMessage());
                            }
//                            Log.d("Search Activity", Integer.toString(tracks.getTotal()));
                            List<Track> track_results = tracks.getItems();
                            LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);

                            for (Track t : track_results)
                            {
                                String artist = "";
                                for (SimpleArtist a : t.getArtists())
                                {
                                    artist += a.getName();
                                }
                                final String final_artist = artist;
                                final String track_name = t.getName();
                                final String track_id = t.getId();
                                try {
                                    Log.d("Search Activity", t.getName() + " by " + artist + " from " + t.getAlbum().getName());
                                }
                                catch (Exception e)
                                {
                                    Log.d("Search Activity", e.getMessage());
                                }
                                final Button track = new Button(getApplicationContext());

                                track.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view)
                                    {
                                        LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                                        View popupView = inflater.inflate(R.layout.submit_song_popup, null);
                                        final PopupWindow popupWindow = new PopupWindow(popupView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                        Button submit = (Button) popupView.findViewById(R.id.buttonSubmitSong);
                                        Button dismiss = (Button) popupView.findViewById(R.id.buttonDismiss);
                                        TextView track_info = (TextView) popupView.findViewById(R.id.textViewTrackInfo);
                                        track_info.setText(track_name + " by " + final_artist);
                                        track_info.setTextColor(getResources().getColor(R.color.white));
                                        submit.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view)
                                            {
                                                RequestQueue queue = Volley.newRequestQueue(getApplicationContext());
//                                                String url ="http://intense-sands-7347.herokuapp.com/addsongtosession/92/" + track_id + "/";
                                                String url = "https://" + base_url + "addsongtosession/" + session_id + "/" + track_id + "/";
                                                Log.d("URL", url);
                                                // Request a string response from the provided URL.
                                                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                                                        new Response.Listener<String>() {
                                                            @Override
                                                            public void onResponse(String response)
                                                            {
                                                                // Display the first 500 characters of the response string.
                                                                Log.d("Search activity", "Response is: "+ response);
                                                            }
                                                        },
                                                        new Response.ErrorListener()
                                                        {
                                                            @Override
                                                            public void onErrorResponse(VolleyError error)
                                                            {
                                                                Log.d("Search Activity", "That didn't work!");
                                                            }
                                                        });
                                                // Add the request to the RequestQueue.
                                                queue.add(stringRequest);

                                            }

                                        });
                                        dismiss.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view)
                                            {
                                                popupWindow.dismiss();

                                            }

                                        });

                                        popupWindow.showAtLocation(search_results_layout, Gravity.CENTER, 0, 0);

                                    }
                                });
                                final LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                track.setText(t.getName() + " by " + artist + " from " + t.getAlbum().getName());
                                track.setTextColor(getResources().getColor(R.color.white));
                                track.setBackgroundColor(getResources().getColor(R.color.background));
                                runOnUiThread(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        search_results_layout.addView(track, lp);
                                    }
                                });

                            }
                        }
                    }).start();


                }
            default:

        }
    }


}

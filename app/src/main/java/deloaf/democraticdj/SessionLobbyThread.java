package deloaf.democraticdj;

import android.os.Looper;
import android.util.Log;

/**
 * Created by mitchkeith145 on 6/10/15.
 */
public class SessionLobbyThread extends Thread implements Runnable
{
    private Session current_session;
    private PostAuthActivity postAuthActivity;
    private boolean mPaused, mFinished;
    private Object mPauseLock;
    public SessionLobbyThread(Session session)
    {
        current_session = session;
        mPaused = false;
        mFinished = false;
        mPauseLock = new Object();
    }

    public void setActivity(PostAuthActivity activity)
    {
        this.postAuthActivity = activity;
    }

    @Override
    public void run()
    {
//        SessionFragment sessionFragment = new SessionFragment();
//        sessionFragment.setArguments(getIntent().getExtras());
//        getSupportFragmentManager().beginTransaction().replace(R.id.main_container, sessionFragment).commit();
        Looper.prepare();
        Log.d("session lobby thread", Thread.currentThread().getName());
        int set_now_playing = 0;
//        while (current_session != null && !mFinished)
        while (current_session != null & ! this.isInterrupted())
        {
            Log.d("session lobby thread", "getting play queue");

            this.postAuthActivity.getQueueIdsSync(this.current_session.getSession_id());
            if (!this.postAuthActivity.isSessionActive() && current_session.isHost()) // if session is inactive and user is host of session
            {
                this.postAuthActivity.start(); // user app should start the session
            }
            else
            {
                Log.d("session lobby thread", "user is not host, or session active, so will not start playing music");
            }
            current_session = this.postAuthActivity.getSession();

            try
            {
                if (current_session.isHost())
                {
                    Log.d("session lobby thread", "sleeping 2.5 seconds");
                    Thread.sleep(2500);
                }
                else
                {
                    if (set_now_playing == 0)
                    {

//                        this.postAuthActivity.getNo();
                        Log.d("session lobby thread", "getting now playing sync");
                        this.postAuthActivity.getNowPlayingSync(this.current_session.getSession_id());
                    }
                    else if (set_now_playing >= 3)
                    {
                        set_now_playing = -1;
                    }

                    set_now_playing++;

                    Log.d("session lobby thread", "not host; sleeping five seconds " + set_now_playing);
                    Thread.sleep(5000);
                }
            }
            catch(InterruptedException e)
            {
                Log.e("session lobby thread", "interrupted exception");
            }

            synchronized (mPauseLock)
            {
                while (mPaused)
                {
                    try
                    {
                        mPauseLock.wait();
                    }
                    catch (InterruptedException e)
                    {
                        Log.e("session lobby thread", "interrupted exception, in synch mPauseLoc");
                    }
                }
            }
        }
    }

    public void onPause()
    {
        Log.d("session lobby thread", "pausing session lobby");
        synchronized (mPauseLock)
        {
            mPaused = true;
        }
    }

    public void onResume()
    {
        Log.d("session lobby thread", "resuming session lobby");
        synchronized (mPauseLock)
        {
            mPaused = false;
            mPauseLock.notifyAll();
        }
    }
}

package deloaf.democraticdj;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;

import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import net.sf.json.JSON;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mitchkeith145 on 6/8/15.
 */
public class JoinSessionFragment extends Fragment implements View.OnClickListener
{
    LinearLayout create_session_container, join_session_container;
    EditText et_name_create, et_name_join;
    Spinner select_threshold, select_timeout;
    Session current_session = new Session();
    private PostAuthActivity attachedActivity;
    private final int CREATE = 0;
    private final int FIND = 1;
    private int create_or_find = CREATE;
    private User user;
    OnSessionChangedListener mCallback;
    Button session1;
    Button session2;
    Button session3;
    Button session4;
    Button session5;
    ScrollView scrollViewSessionResults;
    List<Session> sessions_found = new ArrayList<Session>();
    private String base_url = "intense-sands-7347.herokuapp.com/";
    // private String base_url = "10.0.2.2:5000/";
    public interface OnSessionChangedListener
    {
        public void onSessionCreated(Session session);
        public void onSessionJoined(Session session);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_join, container, false);

        Bundle bundle = this.getArguments();
        int id = bundle.getInt("user_id");
        String username = bundle.getString("user_name");
        user = new User(id, username);
        Button join_session = (Button) v.findViewById(R.id.button_join_session);
        Button create_session = (Button) v.findViewById(R.id.button_create_session);
        Button find_session_name = (Button) v.findViewById(R.id.button_find_session_name);
        Button find_session_gps = (Button) v.findViewById(R.id.button_find_session_gps);
        Button go = (Button) v.findViewById(R.id.button_session_go);
        et_name_create = (EditText) v.findViewById(R.id.editTextCreateName);
        et_name_join = (EditText) v.findViewById(R.id.editTextJoinName);
        select_threshold = (Spinner) v.findViewById(R.id.spinnerSelectThreshold);
        select_timeout = (Spinner) v.findViewById(R.id.spinnerSelectThreshold);
        create_session_container = (LinearLayout) v.findViewById(R.id.create_session_container);
        join_session_container = (LinearLayout) v.findViewById(R.id.join_session_container);
        scrollViewSessionResults = (ScrollView) v.findViewById(R.id.scrollViewSessionResults);
        session1 = (Button) v.findViewById(R.id.button_found_session1);
        session2 = (Button) v.findViewById(R.id.button_found_session2);
        session3 = (Button) v.findViewById(R.id.button_found_session3);
        session4 = (Button) v.findViewById(R.id.button_found_session4);
        session5 = (Button) v.findViewById(R.id.button_found_session5);
        session1.setOnClickListener(this);
        session2.setOnClickListener(this);
        session3.setOnClickListener(this);
        session4.setOnClickListener(this);
        session5.setOnClickListener(this);
        join_session.setOnClickListener(this);
        create_session.setOnClickListener(this);
        find_session_name.setOnClickListener(this);
        find_session_gps.setOnClickListener(this);
        go.setOnClickListener(this);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.number_picker, android.R.layout.simple_spinner_item);
        select_threshold.setAdapter(adapter);
        select_timeout.setAdapter(adapter);

        return v;
    }

    @Override
    public void onAttach(Activity myActivity)
    {
        super.onAttach(myActivity);

        try
        {
            mCallback = (OnSessionChangedListener) myActivity;
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(myActivity.toString() + " must implement OnSessionCreatedListener");
        }
    }



    @Override
    public void onClick(View v)
    {

        switch (v.getId())
        {
            case R.id.button_join_session:
                create_session_container.setVisibility(View.GONE);
                join_session_container.setVisibility(View.VISIBLE);



                break;

            case R.id.button_create_session:
                join_session_container.setVisibility(View.GONE);
                create_session_container.setVisibility(View.VISIBLE);

                break;


            case R.id.button_session_go:
                if (create_or_find == CREATE)
                {
                    String session_name = et_name_create.getText().toString();
                    AsyncHttpClient client = new AsyncHttpClient();

                    LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                    Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    double longitude = location.getLongitude();
                    double latitude = location.getLatitude();

                    String url = "https://" + base_url + "newsession/" + session_name + "/" + user.getUser_id() + "/" + latitude + "/" + longitude + "/";
                    Log.d("session create", url);
                    client.get(url, new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            List<Track> queue;
                            String response = new String(responseBody);
                            try {
                                JSONObject obj = new JSONObject(response);
                                Log.d("create session", response);
                                Session sess = createSessionFromJSON(obj);
                                mCallback.onSessionCreated(sess);
                            } catch (JSONException e) {
                                Log.e("str to json", e.getMessage());
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            Log.d("retrieve queue", error.getMessage());
                        }
                    });
                }
                else if (create_or_find == FIND)
                {

                }
                break;
            case R.id.button_find_session_name:
                et_name_join.setVisibility(View.VISIBLE);
                String session_name = et_name_join.getText().toString();
//                AsyncHttpClient client = new AsyncHttpClient();
//                String url = "http://10.0.2.2:5000/newsession/" + session_name + "/" + user.getUser_id() + "/";
//                client.get(url, new AsyncHttpResponseHandler() {
//                    @Override
//                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//                        List<Track> queue;
//                        String response = new String(responseBody);
//                        try {
//                            JSONObject obj = new JSONObject(response);
//                            Log.d("create session", response);
//                            Session sess = createSessionFromJSON(obj);
//                            mCallback.onSessionCreated(sess);
//                        } catch (JSONException e) {
//                            Log.e("str to json", e.getMessage());
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
//                        Log.d("retrieve queue", error.getMessage());
//                    }
//                });
                break;
            case R.id.button_find_session_gps:
                et_name_join.setVisibility(View.GONE);
                LocationManager lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                double longitude = location.getLongitude();
                double latitude = location.getLatitude();


//                private final LocationListener locationListener = new LocationListener() {
//                    public void onLocationChanged(Location location) {
//                        longitude = location.getLongitude();
//                        latitude = location.getLatitude();
//                    }
//
//                    public void onStatusChanged(String s, int i, Bundle b)
//                    {
//
//                    }
//                }
                Log.d("find session gps", "latitude: " + latitude);
                Log.d("find session gps", "longitude: " + longitude);

                AsyncHttpClient client = new AsyncHttpClient();
                String url = "https://" + base_url + "findsessionsbylocation/" + String.valueOf(latitude) + "/" + String.valueOf(longitude) + "/";
                Log.d("find session loc", url);
                client.get(url, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
                    {
                        List<Track> queue;
                        String response = new String(responseBody);
                        try
                        {
                            JSONObject obj = new JSONObject(response);
                            Log.d("parse sessions", response);
                            sessions_found = parseSessionsFromJSON(obj);
                            scrollViewSessionResults.setVisibility(View.VISIBLE);
                            setSessionsOnUI(sessions_found);
                            for (Session s : sessions_found)
                            {
                                Log.d("session found: ", s.getSession_name() + ", id: " + s.getSession_id());
                            }
//                            mCallback.onSessionCreated(sess);
                        }
                        catch (JSONException e)
                        {
                            Log.e("str to json", e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error)
                    {
                        Log.d("retrieve queue", error.getMessage());
                    }
                });
                break;
            default:
                // the default case is for the buttons pressed to join an existing session
                if (v.getId() == R.id.button_found_session1)
                {
                    Log.d("join session", session1.getText().toString());
                    mCallback.onSessionJoined(sessions_found.get(0));
                }
                else if (v.getId() == R.id.button_found_session2)
                {
                    Log.d("join session", session2.getText().toString());
                    mCallback.onSessionJoined(sessions_found.get(1));
                }
                else if (v.getId() == R.id.button_found_session3)
                {
                    Log.d("join session", session3.getText().toString());
                    mCallback.onSessionJoined(sessions_found.get(2));
                }
                else if (v.getId() == R.id.button_found_session4)
                {
                    Log.d("join session", session4.getText().toString());
                    mCallback.onSessionJoined(sessions_found.get(3));
                }
                else if (v.getId() == R.id.button_found_session5)
                {
                    Log.d("join session", session5.getText().toString());
                    mCallback.onSessionJoined(sessions_found.get(4));
                }
        }
    }

    private List<Session> parseSessionsFromJSON(JSONObject response)
    {
        List<Session> sessions = new ArrayList<Session>();
        try
        {
            JSONArray json_sessions = response.getJSONArray("sessions");
            for (int i = 0; i < json_sessions.length(); i++)
            {
                JSONObject json_session = json_sessions.getJSONObject(i);
                Session session = new Session(json_session.getInt("id"), json_session.getString("name"));
                sessions.add(session);
            }
        }
        catch (JSONException e)
        {
            Log.e("parse json response", e.getMessage());
        }
        return sessions;
    }

    private void setSessionsOnUI(List<Session> sessions)
    {
        final List<Button> button_sessions = new ArrayList<Button>();
        button_sessions.add(session1);
        button_sessions.add(session2);
        button_sessions.add(session3);
        button_sessions.add(session4);
        button_sessions.add(session5);
//        session1.setOnClickListener(this);
//        session2.setOnClickListener(this);
//        session3.setOnClickListener(this);
//        session4.setOnClickListener(this);
//        session5.setOnClickListener(this);
        for (int i = 0; i < 5; i++)
        {
            final int index = i;
            if (i < sessions.size())
            {
                button_sessions.get(i).setText(sessions.get(i).getSession_name());
            }
            else
            {
                button_sessions.get(i).setVisibility(View.GONE);
            }
//            button_sessions.get(i).setOnClickListener(new View.OnClickListener()
//            {
//                @Override
//                public void onClick(View view)
//                {
//                    Log.d("set session ui", button_sessions.get(index).getText() + " has been clicked");
//                }
//            });
        }
    }

    private Session createSessionFromJSON(JSONObject response)
    {
        Session session = null;

        try
        {
            String session_name = response.getString("session_name");
            int id = response.getInt("id");
            session = new Session(id, session_name);
            Log.d("id", String.valueOf(session.getSession_id()));
            Log.d("name", session.getSession_name());
//            JSONArray items = response.getJSONArray("items");
//            for (int i = 0; i < items.length(); i++)
//            {
//                int id = items.getJSONObject(i).getInt("id");
//                String session_name = items.getJSONObject(i).getString("session_name");
//                session = new Session(id, session_name);
//                Log.d("id", String.valueOf(session.getSession_id()));
//                Log.d("name", session.getSession_name());
//            }
        }
        catch (JSONException e)
        {
            Log.e("parse json response", e.getMessage());
        }
        return session;
    }


}

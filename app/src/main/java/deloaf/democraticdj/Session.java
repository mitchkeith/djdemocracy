package deloaf.democraticdj;


import org.joda.time.DateTime;

import java.util.Date;

/**
 * Created by mitchkeith145 on 6/9/15.
 */
public class Session
{
    private int session_id;
    private DateTime last_song_played_at;
    private String session_name;
    private boolean active, is_user_host;
    public Session(int sid, String name)
    {
        session_id = sid;
        session_name = name;
        active = false;
    }

    public Session()
    {
        session_id = -1;
        last_song_played_at = null;
        active = false;
    }

    public void setSessionId(int sid)
    {
        session_id = sid;
    }

    public void setLastSomePlayed(DateTime dt)
    {
        last_song_played_at = dt;
    }

    public int getSession_id() { return this.session_id; }
    public String getSession_name() { return this.session_name; }

    public void setActive(boolean value)
    {
        active = value;
    }

    public boolean isActive()
    {
        return active;
    }

    public void setHost(boolean value)
    {
        is_user_host = value;
    }

    public boolean isHost()
    {
        return is_user_host;
    }
}

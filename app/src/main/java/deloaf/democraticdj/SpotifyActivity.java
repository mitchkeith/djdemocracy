package deloaf.democraticdj;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;
import com.spotify.sdk.android.player.AudioController;
import com.spotify.sdk.android.player.Config;
import com.spotify.sdk.android.player.Spotify;
import com.spotify.sdk.android.player.ConnectionStateCallback;
import com.spotify.sdk.android.player.Player;
import com.spotify.sdk.android.player.PlayerNotificationCallback;
import com.spotify.sdk.android.player.PlayerState;

import java.util.logging.Handler;


public class SpotifyActivity extends Activity implements PlayerNotificationCallback, ConnectionStateCallback {

    // TODO: Replace with your client ID
    private static final String CLIENT_ID = "c4b0ee423fae410e93c948010074a399";
    // TODO: Replace with your redirect URI
    private static final String REDIRECT_URI = "djdemocracy-login-spotify://callback";

    private Player mPlayer;

    private static final int REQUEST_CODE = 42;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spotify);

        AuthenticationRequest.Builder builder = new AuthenticationRequest.Builder(CLIENT_ID, AuthenticationResponse.Type.TOKEN, REDIRECT_URI);

        builder.setScopes(new String[]{"user-read-private", "streaming"});
        AuthenticationRequest req = builder.build();
        AuthenticationClient.openLoginActivity(this, REQUEST_CODE, req);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, Intent intent) {
        Log.d("Spotify", "after authentication");
        super.onActivityResult(requestCode, resultCode, intent);

        // Check if result comes from the correct activity
        if (requestCode == REQUEST_CODE) {
            AuthenticationResponse response = AuthenticationClient.getResponse(resultCode, intent);
            Log.d("Spotify", "request code is correct");
            switch (response.getType()) {
                // Response was successful and contains auth token
                case TOKEN:
                    // Handle successful response
                    Log.d("Spotify", "Successful response");
                    Config playerConfig = new Config(this, response.getAccessToken(), CLIENT_ID);

//                    AudioController ac = new AudioController() {
//                    }
                    Log.d("Spotify", "Config created");


                    final String auth_token = response.getAccessToken();
                    final String client_id = CLIENT_ID;
                    try {
                        mPlayer = Spotify.getPlayer(playerConfig, this, new Player.InitializationObserver() {

                            @Override
                            public void onInitialized(Player player) {
                                Log.d("Spotify", "onInitialized method");
                                mPlayer.addConnectionStateCallback(SpotifyActivity.this);
                                mPlayer.addPlayerNotificationCallback(SpotifyActivity.this);
                                Log.d("Spotify Activity", "launching post auth activity");
                                // launch post authorization activity
                                Intent i = new Intent(getApplicationContext(), PostAuthActivity.class);

                                i.putExtra("client id", client_id);
                                i.putExtra("auth token", auth_token);
                                startActivity(i);
                            }

                            @Override
                            public void onError(Throwable throwable) {
                                Log.e("MainActivity", "Could not initialize player: " + throwable.getMessage());
                            }
                        });
                    } catch (Exception e) {
                        Log.d("Spotify", "Ain't that some shit");
                    }
                    break;

                // Auth flow returned an error
                case ERROR:
                    // Handle error response
                    Log.d("Spotify", "Unsuccessful response");
                    break;

                // Most likely auth flow was cancelled
                default:
                    Log.d("Spotify", "Default response");
                    // Handle other cases
            }
        }
    }

    @Override
    public void onLoggedIn() {
        Log.d("MainActivity", "User logged in");
    }

    @Override
    public void onLoggedOut() {
        Log.d("MainActivity", "User logged out");
    }

    @Override
    public void onLoginFailed(Throwable error) {
        Log.d("MainActivity", "Login failed");
    }

    @Override
    public void onTemporaryError() {
        Log.d("MainActivity", "Temporary error occurred");
    }

    @Override
    public void onConnectionMessage(String message) {
        Log.d("MainActivity", "Received connection message: " + message);
    }

    @Override
    public void onPlaybackEvent(EventType eventType, PlayerState playerState) {
        Log.d("MainActivity", "Playback event received: " + eventType.name());
    }

    @Override
    public void onPlaybackError(ErrorType errorType, String errorDetails) {
        Log.d("MainActivity", "Playback error received: " + errorType.name());
    }

    @Override
    protected void onDestroy()
    {
        Spotify.destroyPlayer(this);
        super.onDestroy();
    }
}